package typemap

import (
	"reflect"
	"testing"
)

type Component struct {
	String string
	Int    int
	Bool   bool
	Struct Struct
}

type Struct struct {
	A, B, C float64
}

func TestNew(t *testing.T) {
	m := New()
	if m == nil || m.m == nil {
		t.Error("typemap was not initialised correctly")
	}
}

func TestMap_Set(t *testing.T) {
	m := New()

	prev, hadPrev := m.Set(&Component{
		String: "hello",
		Int:    1,
		Bool:   true,
		Struct: Struct{
			A: 1,
			B: 2,
			C: 3,
		},
	})
	if hadPrev || prev != nil {
		t.Error("previous value found on newly created map")
	}
}

func TestMap_Has(t *testing.T) {
	m := New()

	_, _ = m.Set(&Component{
		String: "hello",
		Int:    1,
		Bool:   true,
		Struct: Struct{
			A: 1,
			B: 2,
			C: 3,
		},
	})
	ok := m.Has((*Component)(nil))
	if !ok {
		t.Error("added value not found in typemap")
	}
}

func TestMap_Get(t *testing.T) {
	m := New()

	c1 := &Component{
		String: "hello",
		Int:    1,
		Bool:   true,
		Struct: Struct{
			A: 1,
			B: 2,
			C: 3,
		},
	}
	_, _ = m.Set(c1)

	var c2 *Component
	ok := m.Get(&c2)
	if !ok {
		t.Fatal("added value not found in typemap")
	}

	if !reflect.DeepEqual(c1, c2) {
		t.Error("got value not equal to original")
		t.Errorf("%+v", c1)
		t.Errorf("%+v", c2)
	}
}

func TestMap_Delete(t *testing.T) {
	m := New()

	_, _ = m.Set(&Component{
		String: "hello",
		Int:    1,
		Bool:   true,
		Struct: Struct{
			A: 1,
			B: 2,
			C: 3,
		},
	})

	ok := m.Delete((*Component)(nil))
	if !ok {
		t.Fatal("added value not deleted from typemap")
	}

	ok = m.Has((*Component)(nil))
	if ok {
		t.Error("deleted value found in typemap")
	}
}

func BenchmarkMap_Get(b *testing.B) {
	m := New()

	c1 := &Component{
		String: "hello",
		Int:    1,
		Bool:   true,
		Struct: Struct{
			A: 1,
			B: 2,
			C: 3,
		},
	}
	_, _ = m.Set(c1)

	var c2 *Component

	for i := 0; i < b.N; i++ {
		_ = m.Get(&c2)
	}
}
