# Typemap

A zero-dependency package for storing single instances of arbitrary types in a map. Designed for ECS packages
like [WECQS](https://gitlab.com/33blue/wecqs), of questionable usefulness to other types of applications.

Currently this package is being worked on for Go 1.18, and since it uses `unsafe` I cannot guarantee it'll work for
different versions. The minimum Go version in [`go.mod`](/go.mod) will be updated as new versions are released and this
package is tested against them.
