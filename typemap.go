package typemap

import (
	"reflect"
	"unsafe"
)

// Map is a simple structure for storing unique types.
type Map struct {
	m map[uintptr]any
}

// New initialises and returns a new Map.
func New() *Map {
	return &Map{m: make(map[uintptr]any)}
}

// Get gets a value from the map matching the type of dst. It returns true if one was set, else it returns false.
// `v` should be a pointer to the destination variable, and this destination should match the type of whatever value you
// are trying to find. For example, if a (*string) was added to the typemap, `v` should be a (**string).
func (m *Map) Get(v any) bool {
	header := (*_header)(unsafe.Pointer(&v))
	ptrType := (*_ptrtype)(unsafe.Pointer(header.typ))
	u, ok := m.m[uintptr(unsafe.Pointer(ptrType.elem))]
	if !ok {
		return false
	}

	// set v to value of u.
	// TODO: don't use reflect.
	vv := reflect.Indirect(reflect.ValueOf(v))
	vv.Set(reflect.ValueOf(u))

	return true
}

// Set adds the given value to the Map. It returns true and the previous value if one was set, else it returns nil and false.
func (m *Map) Set(v any) (any, bool) {
	header := (*_header)(unsafe.Pointer(&v))
	u, ok := m.m[uintptr(unsafe.Pointer(header.typ))]
	m.m[uintptr(unsafe.Pointer(header.typ))] = v
	return u, ok
}

// Delete deletes a given type from the Map. It returns true if a value was deleted, else it returns false.
func (m *Map) Delete(v any) bool {
	header := (*_header)(unsafe.Pointer(&v))
	_, ok := m.m[uintptr(unsafe.Pointer(header.typ))]
	if !ok {
		return false
	}

	delete(m.m, uintptr(unsafe.Pointer(header.typ)))
	return true
}

// Has returns true if a given type is present in the Map, else it returns false.
func (m *Map) Has(v any) bool {
	header := (*_header)(unsafe.Pointer(&v))
	_, ok := m.m[uintptr(unsafe.Pointer(header.typ))]
	return ok
}

// _header provides type information about any given type.
type _header struct {
	typ *_type
	ptr uintptr
}

type _type struct {
	size       uintptr
	ptrdata    uintptr
	hash       uint32
	tflag      uint8
	align      uint8
	fieldAlign uint8
	kind       uint8
	equal      uintptr
	gcdata     uintptr
	str        int32
	ptrToThis  int32
}

type _ptrtype struct {
	_type
	elem *_type
}
